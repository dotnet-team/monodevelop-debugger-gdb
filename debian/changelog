monodevelop-debugger-gdb (4.0.12-1) unstable; urgency=low

  * [3e33bfb] Fix debian/watch for new upstream tarball naming scheme
  * [4958ff8] Imported Upstream version 4.0.12 (Closes: #708805)
  * [8f7621e] Build-Depend on corresponding version of MonoDevelop
  * [164bf52] delete monodevelop-debugger-gdb.spec in clean rule
  * [6d4c074] Set package dependencies to MD 4.0.12 (Closes: #710761)

 -- Jo Shields <directhex@apebox.org>  Thu, 10 Oct 2013 15:25:23 +0200

monodevelop-debugger-gdb (3.0.3.2-1) unstable; urgency=low

  * [6c614ea] Imported Upstream version 3.0.3.2
  * [b8ba7d3] Bump required MonoDevelop version to 3.0.3.2.
  * [9dc0e1b] Delete RPM spec file in clean rule, since configure generates it.

 -- Jo Shields <directhex@apebox.org>  Sat, 23 Jun 2012 04:33:32 +0100

monodevelop-debugger-gdb (3.0.2-1) unstable; urgency=low

  * [afa0d65] Imported Upstream version 3.0.2
  * [3409b15] Bump MonoDevelop version requirement.

 -- Jo Shields <directhex@apebox.org>  Sun, 27 May 2012 18:27:41 +0100

monodevelop-debugger-gdb (2.8.6.3-1) unstable; urgency=low

  * [73adc5e] Imported Upstream version 2.8.6.3
  * [dc1e0a8] Bump Monodevelop version requirements to 2.8.6.3

 -- Jo Shields <directhex@apebox.org>  Sun, 05 Feb 2012 11:23:03 +0100

monodevelop-debugger-gdb (2.8.5-2) unstable; urgency=low

  * [97cdaa0] Remove gbp.conf since we're using master branch now
  * [a257fbd] Update debian/rules for DH8-compatible "--with cli"

 -- Jo Shields <directhex@apebox.org>  Fri, 20 Jan 2012 10:59:00 +0000

monodevelop-debugger-gdb (2.8.5-1) experimental; urgency=low

  * [008c74a] Imported Upstream version 2.8.5
  * [74bd15b] Bump build dependencies to MonoDevelop 2.8.5.
  * [0272fa8] Bump binary dependencies to MonoDevelop 2.8.5.

 -- Jo Shields <directhex@apebox.org>  Mon, 19 Dec 2011 15:26:09 +0000

monodevelop-debugger-gdb (2.8.2-1) experimental; urgency=low

  * [07a4872] Switch to bz2, not gz, for orig tarball.
  * [3716db7] Imported Upstream version 2.8.2
  * [4cab25d] Bump binary dependency to MonoDevelop 2.8.2.
  * [576aea0] Bump build-dependency to MonoDevelop 2.8.2.

 -- Jo Shields <directhex@apebox.org>  Thu, 10 Nov 2011 17:38:21 +0000

monodevelop-debugger-gdb (2.6-1) experimental; urgency=low

  * [e6f6146] Update debian/watch for Xamarin-hosted source.
  * [917edb6] Imported Upstream version 2.6
  * [e7b5297] Bump build dependancies

 -- Jo Shields <directhex@apebox.org>  Mon, 12 Sep 2011 17:40:10 +0100

monodevelop-debugger-gdb (2.5.92-1) experimental; urgency=low

  * [4b08afd] Update debian/gbp.conf to use upstream-experimental branch
  * [9ec67a2] Imported Upstream version 2.5.92
  * [a92cae2] Refreshed debian/patches/use_csc_compiler.patch.
  * [9a1331c] Don't repack git-orig-source tarball.
  * [bab354e] Move from standalone Quilt to DebSrc 3.0 (quilt).
  * [ee56d2e] Bump Debian Standards Version to 3.9.2.
  * [42c54a3] Bump build-dependencies.

 -- Jo Shields <directhex@apebox.org>  Tue, 28 Jun 2011 23:16:00 +0100

monodevelop-debugger-gdb (2.4-4) experimental; urgency=low

  * [2f2d702] Add a gbp.conf since we're building on a non-default
    branch
  * [014d24e] Refresh packaging to make better use of DH7 abilities,
    especially "--with-quilt"
  * [aafd909] Bump the version number of MonoDevelop required by this
    package to 2.4.1
  * [b5f8d57] Add correct version number on Quilt build-depends, to
    avoid Lintian autoreject

 -- Jo Shields <directhex@apebox.org>  Sat, 18 Dec 2010 15:01:27 +0000

monodevelop-debugger-gdb (2.4-3) unstable; urgency=low

  * Rebuild against MonoDevelop 2.4+dfsg-3, due to a bug in the MonoDevelop
    build system which broke the debugger ABI with every rebuild.

 -- Jo Shields <directhex@apebox.org>  Tue, 14 Sep 2010 22:06:59 +0100

monodevelop-debugger-gdb (2.4-2) unstable; urgency=low

  * Add myself to Uploaders.

 -- Jo Shields <directhex@apebox.org>  Mon, 13 Sep 2010 18:03:54 +0100

monodevelop-debugger-gdb (2.4-1) experimental; urgency=low

  * New upstream release
  * debian/source/format:
    + Force DebSrc 1.0
  * debian/control:
    + Bump build-dep to MonoDevelop 2.4
  * debian/rules:
    + Set MDVERSION to 2.4, for binary dependency
    + Set CSC to mono-csc
  * debian/patches/use_csc_compiler.patch:
    + Refreshed

 -- Jo Shields <directhex@apebox.org>  Wed, 16 Jun 2010 23:46:22 +0100

monodevelop-debugger-gdb (2.2.1-2) unstable; urgency=low

  * Rebuild against MonoDevelop 2.2.2
  * debian/control:
    + Bump MonoDevelop build-dep to 2.2.2
  * debian/rules,
    debian/control:
    + Fiddle build system to put binary-dependencies on MD 2.2.2

 -- Jo Shields <directhex@apebox.org>  Thu, 10 Jun 2010 10:21:40 +0100

monodevelop-debugger-gdb (2.2.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Bump Standards to 3.8.4 (no changes needed)
    + Bump MonoDevelop build-dep to 2.2.1

 -- Jo Shields <directhex@apebox.org>  Fri, 12 Feb 2010 16:31:04 +0000

monodevelop-debugger-gdb (2.2-1) unstable; urgency=low

  * New upstream release

 -- Jo Shields <directhex@apebox.org>  Mon, 11 Jan 2010 15:23:18 +0000

monodevelop-debugger-gdb (2.1.1-1) unstable; urgency=low

  * New upstream release

  * debian/control:
    + Bumped Standards-Version to 3.8.3 (no changes needed)
    + Changed Section to cli-mono to match archive overrides.
    + Bumped mono-devel build-dep to >= 2.4.2.3
    + Added monodevelop-versioncontrol and -nunit to build-deps.
  * debian/patches/use_csc_compiler.patch:
    + Use mono-csc as compiler.

 -- Mirco Bauer <meebey@debian.org>  Sun, 25 Oct 2009 17:38:17 +0100

monodevelop-debugger-gdb (2.0-1) unstable; urgency=low

  [ Jo Shields ]
  * New Upstream Version

  [ Iain Lane ]
  * Bump MD build-dep per configure

 -- Jo Shields <directhex@apebox.org>  Tue, 07 Apr 2009 22:17:27 +0100

monodevelop-debugger-gdb (1.9.3-1) unstable; urgency=low

  * New upstream (bugfix) release
  * Upload to unstable
  * debian/control:
    + Fixed Vcs-* URLs.
    + Bumped monodevelop build-deps to >= 1.9.3
    + Bumped Standards-Version to 3.8.1 (no changes needed)
    + Tighten binary dependency on monodevelop, to ensure the plugin is only
      installed using the same monodevelop version, as the plugins are not
      forward compatible.
  * debian/watch:
    + Updated to use directory of ftp/http download server.

 -- Mirco Bauer <meebey@debian.org>  Sun, 22 Mar 2009 22:26:37 +0100

monodevelop-debugger-gdb (1.9.2-1) experimental; urgency=low

  * Initial release. (Closes: #516435)

 -- Mirco Bauer <meebey@debian.org>  Sun, 15 Feb 2009 22:32:12 +0100
